
\section{Background} \label{Background}

This section presents the underlying concepts in our proposal. Section~\ref{Metamodel} provides an overview of the METAKIP metamodel; Section~\ref{Automated Planning} introduces basic concepts of automated planning; Section~\ref{MDP} explains Markov decision process (MDP).  Section~\ref{PRISM} describes the PRISM tool and language. 

\subsection{METAKIP: A Metamodel for KiPs Definition} \label{Metamodel}

Our previous work proposed an artifact-centric metamodel~\cite{venero2019towards} for the definition of KiPs, aiming to support knowledge workers during the decision-making process. The metamodel supports data-centric process management, which is based on the availability and values of data rather than completion of activities. In data-centric processes, data values drive decisions and decisions dynamically drive the course of the process~\cite{reichert2012enabling}. The metamodel is divided into four major packages: case, control-flow, knowledge, and decision,  in such a way that there is an explicit integration of the data, domain, and organizational knowledge, rules, goals, and activities. %Figure~\ref{fig_Metamodel} shows the relationship among the defined components. 

\begin{comment}

\begin{figure*}[htp!]
\centering
\includegraphics[width=4in]{figures/Metamodel.png}
\caption{Metamodel for KiPs [ref paper]}
\label{fig_Metamodel}
\end{figure*}

\end{comment}

The Case Package defines the base structure of the metamodel, a \emph{Case}. A case model definition represents an integrated view of the context and environment data of a case, following the artifact-centric paradigm. This package is composed of a set of interconnected artifacts representing the logical structure of the business process. An \emph{artifact} is a data object composed of a set of items, attributes, and data values, defined at run time. 
%Artifacts represent daily documents used by the organization. These artifacts have some properties and relations representing the logical structure of the business process. An artifact is a data object composed by a set of items, which represents a section in a document. Each item has atomic and structured attributes, which are related to activities. At runtime, data values are filled in the artifacts by activities which allows enabling other activities.
 
%The Knowledge Package represents the domain and organizational rules, norms, guidelines, best practices and standards. This 
The Knowledge Package captures explicit organizational knowledge, which is encoded through \emph{tactic templates}, \emph{goals}, and \emph{metrics} that are directly influenced by business rules. Tactics templates represent best practices and guidelines. Usually, they have semi-structured sequences of activities or unstructured loose alternative activities pursuing a goal. %This package is intrinsically related to the control-flow package. 
      
The Control-flow Package defines the \emph{behavior} of a case. It is composed of a set of data-driven activities to handle different cases. Activity definitions are made in a declarative way and have \emph{pre-} and \emph{post-conditions}. The metamodel refines the granularity of an \emph{activity} that could be a step or a task. A \emph{task} is logically divided into \emph{steps}, which allows better management of data entry on the artifacts. Step definitions are associated with a single attribute of an artifact, a resource, and a role type at most. This definition gives us a tight integration between data, steps and resources. 
%The state transitions for activities, tasks, steps enable the monitoring of the course of the process and data value updates inside the context and environment artifacts. Step definitions are associated with a single attribute of an artifact, a resource, and a role type at most. This definition gives us a tight integration between data, steps and resources. 
 
These packages are used to model alternative plans to answer emergent circumstances, reflecting environmental changes or unexpected outcomes during the execution of a KiP. The Decision Package represents the structure of a collaborative decision-making process performed by knowledge workers. We proposed a representation of how decisions can be made by using the principles of strategic management, such as, looking towards goals and objectives and embracing uncertainty by formulating strategies for the future and correct them if necessary. The strategic plan is structured at run time by goals, objectives, metrics and tactic templates. %by defining the process model to coordinate the process. 


\subsection{Automated Planning} \label{Automated Planning}

Planning is the explicit and rational deliberation of actions to be performed to achieve a goal~\cite{ghallab2004automated}. The process of deliberation consists of choosing and organizing actions considering their expected outcomes in the best possible way. Usually, planning is required when an activity involves new or less familiar situations, complex tasks and objectives, or when the adaptation of actions is constrained by critical factors such as high risk. Automated planning studies the deliberation process computationally~\cite{ghallab2004automated}.

A conceptual model for planning can be represented by a state-transition system, which formally is a 4-tuple $\Sigma = (S, A, E, \gamma)$, where $S=\{s_{1}, s_{2}, . . . . \}$ is a finite or recursively enumerable set of states; $A = \{a_{1}, a_{2},...\}$ is a finite or recursively enumerable set of actions; $E = \{e_{1}, e_{2},...\}$ is a finite or recursively enumerable set of events; and $\gamma: S \times A \times E \rightarrow 2^{S}$ is a state-transition function.

Actions are transitions controlled by a plan executor. Events are unforeseen transitions that correspond to the internal dynamics of the system and cannot be controlled by the plan executor. Both events and actions contribute to the evolution of the system. Given a state transition system $\Sigma$, the purpose of planning is to deliberate which actions to apply into which states to achieve some goal from a given state. A plan is a structure that gives the appropriate actions. 

\subsection{Markov Decision Process (MDP)} \label{MDP}

A Markov decision process (MDP) is a discrete-time stochastic control process. It is a popular framework designed to make decisions under uncertainty, dealing with nondeterminism, probabilities, partial observability, and extended goals~\cite{ghallab2004automated}.

\begin{comment}
Many real-world planning problems occur in very dynamic environments where actions may not always have the expected outcomes due to new emergent events or new information arrival which lead to many different execution paths. Modeling all possible execution paths is not realistic since it is impossible to predict everything that could happen. Planning with non-deterministic domains requires ways to analyze all possible action effects and generate plans that have conditional behaviors. Some action effects are more likely to happen than others, so, it is possible to model action outcomes with associated probabilities to deal with the uncertainty. 
\end{comment}

In MDPs, an agent chooses action $a$ based on observing state $s$ and receives a reward $r$ for that action~\cite{kochenderfer2015decision}. The state evolves probabilistically based on the current state and the action taken by the agent. 

Figure~\ref{fig_MDP}(a) presents a decision network~\cite{kochenderfer2015decision}, used to represent a MDP. The state transition function $T({s}'|s,a)$ represents the probability of transitioning from state $s$ to ${s}'$ after executing action $a$. The reward function $R(s,a)$ represents the expected reward received when executing action $a$ from state $s$. We assume that the reward function is a deterministic function of $s$ and $a$.

\begin{figure}[htp!]

\begin{tabular}{m{16em}m{20em}}
\centering
\includegraphics[width=2in]{figures/MDP.png} & 
\includegraphics[width=3in]{figures/prism.png} \\
\centering (a) & \centering  (b)
\end{tabular}
\caption{(a) MDP representation \cite{kochenderfer2015decision} and (b) Example syntax of \textit{mdp} PRISM \cite{KNP11} module and rewards}
\label{fig_MDP}
\end{figure}

An MDP treats planning as an optimization problem in which an agent needs to plan a sequence of actions that maximizes the chances of reaching the goal. Action outcomes are modeled with a probability distribution function. Goals are represented as utility functions that can express preferences on the entire execution path of a plan, rather than just desired final states. For example, finding the optimal choice of treatment optimizing the life expectancy of the patient or optimizing cost and resources.

\subsection{PRISM} \label{PRISM}

PRISM~\cite{KNP11} is a probabilistic model checker that allows the modeling and analysis of systems that exhibit probabilistic behavior. The PRISM tool provides support for modeling and construction of many types of probabilistic models: discrete-time Markov chains (DTMCs), continuous-time Markov chains (CTMCs), Markov decision processes (MDPs), and probabilistic timed automata (PTAs). The tool supports statistical model checking, confidence-level approximation, and acceptance sampling with its discrete-event simulator. 
For non-deterministic models it can generate an optimal adversary/strategy to reach a certain state. 

\begin{comment}
To analyze probabilistic models, the tool provides support for automated analysis of a wide range of quantitative properties. The property specification language incorporates the temporal logics PCTL(Probabilistic Computation Tree Logic ), CSL (continuous stochastic logic), LTL(Linear temporal logic) and PCTL* (which subsumes both PCTL and LTL), as well as extensions for quantitative specifications and costs/rewards. PCTL is used for specifying properties of DTMCs, MDPs or PTAs; CSL is an extension of PCTL for CTMCs; LTL and PCTL* can be used to specify properties of DTMCs and MDPs (or untimed properties of CTMCs). PRISM supports most of the (non-probabilistic) temporal logic CTL.
\end{comment}

Models are described using the PRISM language, a simple, state-based language based on the reactive modules formalism~\cite{Alur:1999:RM:325887.325890}. Figure~\ref{fig_MDP}(b) presents an example of the syntax of a PRISM module and rewards. The fundamental components of the PRISM language are modules. A module has two parts: variables and commands. Variables describe the possible states that the module can be in at a given time. Commands describe the behavior of a module, how the state changes over time. A command comprises a guard and one or more updates. The guard is a predicate over all the variables in the model. Each update describes a transition that the module can take if the guard is true. A transition is specified by giving the new values of the variables in the module. Each update has a probability which will be assigned to the corresponding transition. Commands can be labeled with actions. These actions are used for synchronization between modules. Cost and rewards are expressed as real values associated with certain states or transitions of the model. 

\begin{comment}

\begin{figure}[]
\centering
\includegraphics[width=3 in]{figures/prismSyntax.png}
\caption{Example syntax of \textit{MDP} PRISM module and rewards}
\label{fig_MDP}
\end{figure}

A model is composed of a number of modules which can interact with each other. The global state of the whole model is determined by the local state of all modules. 

PRISM has been widely used in many application domains, such as wireless communication protocols, quantum cryptography, biology systems, among others.
\end{comment}
