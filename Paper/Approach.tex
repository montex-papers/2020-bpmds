\section{Dynamic Plan Generation for KiPs Execution} \label{Methodology}

\begin{comment}

The solution for generating run-time plans is based on an intelligent agent that suggests solution paths to support knowledge workers' decision-making process. The agent makes decisions based on a knowledge base and creates a plan towards a goal state. Thereby, process modeling involves both the intelligent agents and knowledge workers in a constant interleaving of planning, execution (configuration and enactment), plan supervision, plan revision, and replanning.

\end{comment}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 In our approach, plans are fragments of process models that are frequently created and modified during process execution. Plans may change as new information arrives and/or when a new goal is set. We advocate the creation of a planner to structure process models at run time based on a knowledge base. The planner synthesizes plans on-the-fly according to ongoing circumstances. The generated plans should be revised and re-planned as soon as new information becomes available. Thereby, it involves both computer agents and knowledge workers in a constant interleaving of planning, execution (configuration and enactment), plan supervision, plan revision, and re-planning. An interactive software tool might assist human experts during planning. This tool should allow defining planning goals and verifying emerging events, states, availability of activities and resources, as well as preferences. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Model Formulation}

The run-time generation of planning models according to a specific situation in a case instance requires the definition of the planning domain and then the planning problem itself. 

\begin{definition}
Let the case model be represented according to the METAKIP metamodel. The planning domain is derived from the case model that can be described using a state-transition system defined as a 5-tuple $\Sigma = (S, A, E, \gamma, C)$ such as that: $S$ is the set of possible case states. $A$ is the set of actions that are represented by activities inside tactics that an actor may perform. $E$ is the set of events in the context or in the environment. $ \gamma: S \times A \times E \to 2^{S}$, is the state-transition function, so the system evolves according to the actions and events that it receives. $C: S \times A \to [0,\infty)$ is the cost function that may represent monetary cost, time, risk or something that can be minimized or maximized. 

\begin{comment}

\begin{itemize}
    \item $S$ is the set of possible case states.
    \item $A$ is the set of actions that are represented by activities inside tactics that an actor may perform.
    \item $E$ is the set of events in the context or in the environment.
    \item $ \gamma: S \times A \times E \to 2^{S}$, is the state-transition function, so the system evolves according to the actions and events that it receives.
    \item $c: S \times A \to [0,\infty)$ is the cost function that may represent monetary cost, time or something that can be minimized or maximized. 
    \end{itemize}

\end{comment}

\end{definition}

The state of a case is the set of values (available data) of the attributes contained in artifacts of the \textit{context} and the \textit{environment}. However, since the number of attributes of the artifacts is very large, it is necessary to limit the number of attributes to only the most relevant ones, which determines the current state of the case at a given time $t$.

\begin{definition}
A state $s_t$ is the set of values corresponding to a set of relevant attributes  $\{ v_{1}, v_{2}, \dots v_{r} \} $,  with $r \ge 1$,  contained in the business artifacts at a given time $t$. 
\end{definition}

Actions in the METAKIP metamodel are represented by the activities within a tactic. Tactics represent best practices and guidelines used by the knowledge workers to make decisions. In METAKIP, they serve as tactic templates to be instantiated to deal with some situations during the execution of a case instance. Tactics are composed of a finite set of activities pursuing a goal. A tactic can be structured or unstructured. A tactic is a 4-tuple $T = (G, PC, M, A)$, where: $G$ is a set of variables representing the pursuing goal state, $PC$ is a finite set of preconditions representing a state required for applying the tactic, $M$ is a set of metrics to track and assess the pursuing goal state, and $A$ is a finite set of activities. 

In METAKIP, an activity could be a single step or a a set of steps (called a task). An activity has some preconditions and post-conditions (effects). We map activities into executable actions. An executable action is an activity in which their effects can modify the values of the attributes inside business artifacts. These effects can be deterministic or non-deterministic. 

\begin{definition}
 An action is a 4-tuple $a = (Pr,\mathit{Eff},Pb, c)$ where: $Pr$ is a finite set of preconditions. $\mathit{Eff}$ is a finite set of effects. $Pb$ is a probability distribution on the effects, such that, $P_{ef}({i})$ is the probability of  effect $ef  \in  \mathit{Eff}$ and $\sum_{ef \in \mathit{Eff}} P_{ef}({i}) =1$. $c$ is the number which represents the cost (monetary, time, etc.) of performing $a$.
\end{definition}

As the state-transition function $\gamma$ is too large to be explicitly specified, it is necessary to represent it in a generative way. For that, we use the planning operators from which it is possible to compute $\gamma$. Thus, $\gamma$ can be specified through a set of planning operators $O$. A planning operator is instantiated by an action.

\begin{definition}
 
 A planning operator $O$ is a pair $(id, a)$ where $a$ is an action and $id$ is a unique identifier of action $a$.

\end{definition}
 
At this point, we are able to define the planning problem to generate a plan as a process model.

\begin{definition}
The planning problem for generating a process model at a given time $t$ is defined as a triple $P = (OS_{t},GS_{t}, RO_{t})$, where: $OS_{t}$ is the observable situation of a case state at time $t$. $GS_{t}$ is the goal state at time $t$, a set of attributes with expected output values. $RO_{t}$ represents a subset of the $O$ that represents only available and relevant actions for a specific situation during the execution of a case instance at a given time $t$. 
\end{definition}

\begin{definition}
The observable situation of a case instance $C$ state at a given time $t$  is a set of attributes $OS_{t} \ = \{ v_1, v_2, \dots, v_m \}$, with $m\ge 1$, such that $v_i \in  \ S_t \cup I_t$ for each $1 \le i \le m$, where the state of $C$ is $S_t$ and the set issues in the situation of $C$ is $I_t$. 
\end{definition}

\begin{definition}
The goal state of an observable situation of case instance $C$  at a given time $t$ is the set of attributes $GS_t = \{ v_1,v_2, \dots, v_m \}$,  with $m \ge 1$, such that, for $1 \le i\le m$, $v_i$ is an attribute with an expected output value, $v_i$ belongs to an artifact of $C$. These attributes are selected by the knowledge workers. Some metrics required to asses some goals inside tactics can be added to the goal. $GS_t$ represents the expected reality of $C$. 
\end{definition}

$GS_t$ serves as an input for searching an execution path for a specific situation. Different goal states can be defined over time.

\begin{definition}
Let $P = (OS_{t},GS_{t}, RO_{t})$ be the planning problem. A plan $\pi$ is a solution for $P$. The state produced by applying $\pi$ to a state $OS_{t}$ in the order given is the state $GS_{t}$. A plan is any sequence of actions $\pi$ = $(a_{1}, . . . ,a_{k})$, where $k \ge 1$. The plan $\pi$ represents the process model.
\end{definition}

%Defining the problem in that way, it is possible 
Our problem definition enables the use of different planning algorithms and the application of automatic planning tools to generate alternatives plans. As we are interested in KiPs, which are highly unpredictable processes, we use Markov Decision Processes for formulating the model for the planner. MDPs allows us to represent uncertainty with a probability distribution. MDP makes sequential decision making and reasons about the future sequence of actions and obstructions, which provides us with high levels of flexibility in the process models. In the following, we show how to derive an MDP model expressed in the PRISM language from a METAKIP model automatically.

\subsection{PRISM Model Composition}
Algorithm~\ref{algorithm: PRISMGenerator} shows the procedure to automatically generate the MDP model for the PRISM tool, where the input parameters are: $OS_t$, $GS_t$, set of domain $Tactics$, $t$ is the given time, $PP$ minimum percentage of preconditions satisfaction, and $PG$ minimum percentage of goal satisfaction, both $PP$ and $PG$ are according to the rules of the domain. 
As described in Section~\ref{PRISM}, a module is composed of variables and commands. Variables of the module are the set of attributes from the case artifacts that belong to $OS_t \cup GS_t$. Commands are represented for the relevant planning operators $RO_t$. The name of the command is the identifier of the action, the guards are the preconditions $PC$ and the effects $\mathit{Eff}$ are the updates with associated probabilities. Rewards are represented by the cost of actions $c$ and are outside of the module of PRISM. 

\begin{algorithm}
\scriptsize
\label{Procedure}
    \begin{algorithmic}%[1]
        \caption{PRISM Model Generator} \label{algorithm: PRISMGenerator}
        \Require $OS_t, \ GS_t,\ Tactics, \ t, \ PP, \ PG  $ 
        \State $V \gets OS_t \cup GS_t$ \Comment{Attributes of $OS_t \ and \ GS_t$ correspond to PRISM variables}
        \ForAll { $T \in \ Tactics$ } \Comment{For each tactic} 
            \State $p_1 \gets |T.PC \cap OS_t| / |T.PC|$ \Comment{Percentage of satisfied preconditions}
            \State $p_2 \gets |GS_t \cap T.G| / |GS_t|$ \Comment{Percentage of achievable target goal}
            \If{$p_1 \geq PP$ and $p_2 \ge PG$} \Comment{If percentages are acceptable}
             \State $ ST \gets ST \cup T$ \Comment{ Add to the set of selected tactics  }  
            \EndIf
        \EndFor
        \State $ RT \gets SelectRevevantTactics(ST)$ \Comment{Relevant tactics for the current situation $OS_t$}
        \State $A_t \gets CheckAvailableActivities(RT,t)$ \Comment{Select available activities at time t}
        \State $RO_t \gets   CreatePlanningOperators(A_t)$ 
        \State $C \gets   CreateCommands(RO_t)$ 
        \State $R \gets   CreateRewards(RO_t)$ 
        \State $V \gets V \cup \{T.M : T \in RT\}$\Comment{Add necessary metrics to evaluate}
        \State $CreatePRISMModel(V,C,R)$
    \end{algorithmic}
\end{algorithm}

For finding the set of relevant planning operators $RO_t$, first, we select tactics whose preconditions must be satisfied by the current situation $OS_t$ and whose goal is related to the target state $GS_{t}$. This can be done by calculating the percentages of both the satisfied preconditions and achievable goals. If these percentages are within an acceptable range according to the rules of the domain, the tactics are selected. Second, this first set of tactics is shown to the knowledge workers who select the most relevant tactics. The set of the selected relevant tactics is denoted as $RT$. From this set of tactics, we verify which activities inside the tactics are available at time $t$. Thus, the set of available actions at time $t$ is denoted by $A_{t}={a_{1},a_{2}, \dots,a_{n}}$. Finally, the relevant planning operators, $RO_t$, are created by means of $A_t$.

\begin{comment}
First, we need to acquire the variables for the module. For that, we get the attributes from $OS_t$ and $GS_t$, which usually are going to be the same. 
Second, we need to find relevant tactics for that specific situation and target goal in order to find relevant actions for creating the relevant planning operators $RO_t$. For that, first, we select the tactics whose preconditions must be satisfied by the current situation $OS_t$ and whose goal is related to the target state $GS_{t}$. This can be done by calculating the percentages of both the satisfied preconditions and achievable goals. If these percentages are within an acceptable range according to the rules of the domain, the tactics are selected. This first set of relevant tactics is shown to the knowledge workers who select the most relevant tactics. The set of the selected relevant tactics is denoted as $RT$. From this set of tactics, it is necessary to verify which executable activities inside of them are available at time $t$. The set of available actions at time $t$ is denoted by $A_{t}={a_{1},a_{2}, \dots,a_{n}}$. At this point, we can create the set of relevant planning operators $RO_t$. 

Third, create the PRISM commands from the planning operators $RO_t$. In the PRISM language, the planning operators are represented by commands. The name of the command is the name of the action, the guards are the preconditions $precond$ and the effects $Effects$ are the updates with associated probabilities. 
Fourth, create rewards from the planning operators.
Fifth, we must update the variables of the module with the required metrics to asses the tactics.
Sixth, initialize values of the variables for the module.
Finally, deploy the PRISM model.   
\end{comment}

\subsection{Plan Generation}

To generate plans in PRISM, it is necessary to define a property file that contains properties that define goals as utility functions. PRISM evaluates properties over an MDP model and generates all possible resolutions of non-determinism in the model, state graphs, and gives us the optimal state graph. The state graph describes a series of possible states that can occur while choosing actions aiming to achieve a goal state. It maximizes the probability to reach the goal state taking into consideration rewards computed, that is maximizing or minimizing rewards and costs.

In our context, a property represents the goal state $GS_t$ to be achieved while trying to optimize some criteria. Then, PRISM calculates how desirable an executing path is according to one criterion. Thus, plans can be customized according to knowledge workers' preferences (costs and rewards). To generate a plan, we need to evaluate a property. The generated plan is a state graph that represents a process model to be executed at time $t$. The generated process model shows case states as nodes and states transitions as arcs labeled with actions which outcomes follow probability distribution function. According to this state graph, the knowledge worker could choose which action to execute in a particular state. This helps knowledge workers to make decisions during KiPs execution.


